var app = require('./config/app_config'); //incluindo o arquivo config 
var dados = require('./models/dados'); //incluindo a parte dos dados 
var dadosController = require('./controllers/dadosController');

app.get('/', function(req, res){
    res.end('API - Coordenadas das Estacoes de Trem(Supervia - RJ)');
});

//rotas para os dados das estacaoes
app.get('/dados/:linha', function(req,res){
    var linha = req.params.linha;

    dadosController.getDados(linha, function(resp){
        res.json(resp);
    });
});

