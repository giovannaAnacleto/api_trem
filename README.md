#TP3 - Desenvolvimento Agil


A API retorna as coordenadas geograficas(longitude e latitude) da Estação de Trem(Supervia - RJ)

Modulos Utilizados:

**Express (Criação de APIS)**

    `$ ~ npm install express --save`

**Body Parser (Parser no Json)**

    `$ ~ npm install body-parser --save`

**Request (Consumir a API)**

    `$ ~ npm install request --save`

**Nodemon (Run na API)**

    `$ ~ npm install nodemon --save`

Comandos previos para startar a aplicação:

- Para fazer a instalação de todos os pacotes do package.json rodar o seguinte comando no prompt ou terminal:

    **OBS: É necessario ser administrador/root para rodar o comando**

    `# ~ npm install`

- Após isso irá gerar uma pasta chamada "node_modules" com os pacotes necessários para a aplicação.


Para rodar o projeto:

 `$ ~ cd caminho_ate_a_pasta_raiz`

 `$ ~ nodemon`

Como usar:

Ao levantar o serviço da API, ir em um browser e digitar na URL:

  - localhost:7600/ - para rota default

  - localhost:7600/dados/nome_da_estacao - para retornar as coordenadas da estacao  


