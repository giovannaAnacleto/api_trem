var express = require('express');
var bodyParser = require('body-parser');

var port = '7600';
var app = module.exports = express();

app.listen(port);
app.use(bodyParser.urlencoded({encoded:true}));
app.use(bodyParser.json());

//configuracoes de uso da API 
app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin', '*'); 
    res.setHeader('Acces-Control-Allow-Methods', 'GET,POST,PUT,DELETE'); 
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization'); 
    next();
});
