var request = require('request');
var http = require('http');

var retorno;
request(
    'http://pgeo3.rio.rj.gov.br/arcgis/rest/services/Transporte_Trafego/Transporte_Publico/MapServer/16/query?where=1=1&outFields=*&outSR=4326&f=json',
    function  (err,res,body) {
    retorno =  body;
});

exports.getEstacoes = function(nomeEstacao){
    var objeto = JSON.parse(retorno);
    
    // console.log(objeto.features.length);
    // console.log(nomeEstacao);
    
    for(var i = 0; i < objeto.features.length; i++){
        
        if(objeto.features[i].attributes.Nome == nomeEstacao){
            return objeto.features[i].geometry;
        }else{
            continue;
          
        }
    }
}